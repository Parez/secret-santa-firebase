import { SantaPage } from './app.po';

describe('santa App', () => {
  let page: SantaPage;

  beforeEach(() => {
    page = new SantaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
