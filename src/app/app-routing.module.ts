import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {DashboardPageComponent} from './dashboard-page/dashboard-page.component';
import {AuthGuard} from './guards/auth.guard';
import {CanDeactivateHomeGuard} from './guards/can-deactivate-home.guard';
import {GroupPageComponent} from './group-page/group-page.component';
import {GroupsListPageComponent} from './groups-list-page/groups-list-page.component';
import {ProfilePageComponent} from './profile-page/profile-page.component';
import {UserInGroupGuard} from './guards/user-in-group.guard';


const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomePageComponent },
  { path: 'profile', pathMatch: 'full', component: ProfilePageComponent, canActivate: [AuthGuard] },
  { path: 'groups', pathMatch: 'full', component: GroupsListPageComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', pathMatch: 'full', component: DashboardPageComponent, canActivate: [AuthGuard] },
  { path: 'group/:id', pathMatch: 'full', component: GroupPageComponent, canActivate: [AuthGuard, UserInGroupGuard] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
