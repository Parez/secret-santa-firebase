import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Observable} from 'rxjs/Observable';
import {MatDialog} from '@angular/material';
import {CreateGroupDialogComponent} from '../components/create-group-dialog/create-group-dialog.component';
import {GroupsService} from '../services/groups.service';
import {Group} from '../models/group.model';
import {JoinGroupDialogComponent} from '../components/join-group-dialog/join-group-dialog.component';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  public userName$: Observable<string>;
  public groups$: Observable<Group[]>;
  constructor(public authService: AuthService,
              public groupsService: GroupsService,
              public dialog: MatDialog) {
    this.userName$ = this.authService.user$.map(user => {
      if (user) {
          return user.displayName
      } else {
        return '';
      }
    });
  }

  ngOnInit() {
      this.groups$ = this.groupsService.getUserGroups();
  }

  onCreateGroupClick() {
      const dialogRef = this.dialog.open(CreateGroupDialogComponent);

      dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
      });
  }

  onJoinGroupClick() {
    const dialogRef = this.dialog.open(JoinGroupDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
    });
  }


}
