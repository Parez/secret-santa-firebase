import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-base-page',
  templateUrl: './base-page.component.html',
  styleUrls: ['./base-page.component.scss']
})
export class BasePageComponent {

    public userName$: Observable<string>;
    constructor(public authService: AuthService) {
        this.userName$ = this.authService.user$.map(user => {
            if (user) {
                return user.displayName
            } else {
                return '';
            }
        });
    }

    onLogout() {
        this.authService.logOut();
    }
}
