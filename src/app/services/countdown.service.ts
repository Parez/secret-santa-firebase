import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import * as moment from 'moment';
import {Duration} from 'moment';
import * as firebase from 'firebase';

@Injectable()
export class CountdownService {

  public countdown$: Observable<Duration> = new Observable();
  public end: number = 0;
  private serverOffset: number;
  constructor() {

      this.end = moment('01122018', 'DDMMYYYY').valueOf();

      const offset = new Date().getTimezoneOffset();
      console.log(offset);

      this.countdown$ = Observable.timer(0, 1000)
          .map(time => moment.duration(this.end - Date.now()));
  }

  private getLocalTime() {

  }



}
