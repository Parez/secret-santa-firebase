import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {AngularFireDatabase} from 'angularfire2/database';
import {User} from '../models/user.model';

@Injectable()
export class AuthService {

    public user$: Observable<any>;
    public isLoggedIn$: Observable<boolean> = new BehaviorSubject(false);
    public loggedInId: string;
    public loggedInUser: User;
    constructor(private afAuth: AngularFireAuth,
                private router: Router,
                private afDatabase: AngularFireDatabase) {
        this.user$ = this.afAuth.authState
            .do(user => this.loggedInId = user ? user.uid : '')
            .do(user => this.loggedInUser = user ? {displayName: user.displayName, email: user.email, id: user.uid} : null)
            .switchMap(user => {
                if (user) {
                    return this.afDatabase.object(`/users/${user.uid}`).valueChanges()
                } else {
                    return Observable.of(null);
                }
            });
        this.isLoggedIn$ = this.user$.map(user => user != null);
    }

    signInFacebook() {
      this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((userData) => {
          this.saveUser(userData.user.uid, userData.user.email, userData.user.displayName);
          this.router.navigate(['groups']);
      });
    }

    signInGoogle() {
        this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then((userData) => {
            this.saveUser(userData.user.uid, userData.user.email, userData.user.displayName);
            this.router.navigate(['groups']);
        });
    }

    signInEmail(email, password, displayName = '') {
        this.afAuth.auth.signInWithEmailAndPassword(email, password).then(() => {
            this.router.navigate(['groups']);
        });
    }

    registerByEmail(email, password, displayName) {
        this.afAuth.auth.createUserWithEmailAndPassword(email, password).then( (userData) => {
            console.log(userData);
            this.saveUser(userData.uid, userData.email, displayName);
            this.router.navigate(['groups']);
        })
    }

    updateProfileInfo(info: {displayName: string, interests: string}) {
        this.afDatabase.object(`/users/${this.loggedInId}`).update(info);
    }

    getProfileInfo(): Observable<User> {
        return this.afDatabase.object(`/users/${this.loggedInId}`).valueChanges();
    }

    getUserInfo(userId: string): Observable<User> {
        return this.afDatabase.object(`/users/${userId}`).valueChanges();
    }

    private saveUser(id, email, displayName = '') {
        const user: User = {email, displayName};
        return this.afDatabase.object(`/users/${id}`).valueChanges()
            .map(userData => userData !== null)
            .do(current => console.log('User', current))
            .subscribe(exists => {
                if (!exists) {
                    this.afDatabase.object(`/users/${id}`).set(user);
                }
            });
    }

    logOut() {
      this.afAuth.auth.signOut().then(() => {
          this.router.navigate(['']);
      });
    }

}
