import { Injectable } from '@angular/core';
import {Group} from '../models/group.model';
import {AngularFireDatabase, QueryFn} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './auth.service';
import {User} from '../models/user.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class GroupsService {

    constructor(private afDatabase: AngularFireDatabase, private authService: AuthService) { }

    getGroupNames(start: BehaviorSubject<string>, end: BehaviorSubject<string>): Observable<string[]> {
        return Observable.zip(start, end).switchMap(params => {
            if (params[0].length < 1) {
                return Observable.of([]);
            }

            return this.afDatabase.list('/group_names', ref => {
                return ref.orderByValue().limitToFirst(10).startAt(params[0]).endAt(params[1])
            }).valueChanges().map(vals => {
                return vals.map(val => val.toString())
            })
        });
    }

    isCurrentUserInGroup(groupId: string, access: string) {
        const user = this.authService.loggedInId;
        return this.afDatabase.object(`/user_groups/${user}/${access}/${groupId}`).valueChanges()
            .map(value => {
                return value != null;
            }).publishReplay(1).refCount();
    }

    getGroupAccess(groupId: string): Observable<string> {
        return this.afDatabase.object(`/group_access/${groupId}`).valueChanges()
            .map(value => value.toString());
    }

    getAllGroups(): Observable<Group[]> {
        return this.afDatabase.list(`/group_access/`).snapshotChanges()
            .map(snapshots => snapshots.map(snapshot => {
                return { key: snapshot.key, value: snapshot.payload.val() };
            }))
            .switchMap((groupIds) => {
                console.log(groupIds);
                return Observable.combineLatest(groupIds.map(id => {
                    console.log(`/${id.value}_groups/${id.key}/metadata`);
                    return this.afDatabase.object(`/${id.value}_groups/${id.key}/metadata`).valueChanges()
                        .map(value => {
                            return {key: id.key, value};
                        })
                }));
            }).map((objs: any[]) => {

                return objs.map(obj => {
                    return {
                        id: obj.key,
                        count: obj.value.count,
                        owner: obj.value.owner,
                        name: obj.value.name,
                        price: obj.value.price,
                        access: obj.value.access,
                        status: obj.status
                    }
                })
            });
    }

    createGroup(name: string, password: string, price: number) {
        const owner = this.authService.loggedInUser;
        const access = password.length > 0 ? 'private' : 'public';
        const groupsList = this.afDatabase.list(`/${access}_groups/`);
        groupsList.push(
            {
                metadata: {
                    owner,
                    price,
                    name,
                    count: 0,
                    access,
                    status: 'pending'
                },
                password
            }
        ).then((createdGroup) => {
            this.updateUserGroups(owner.id, createdGroup.key, password, true);
            this.updateGroupUsers(owner.id, createdGroup.key, password, true);
            this.pushGroupName(createdGroup.key, name);
            this.pushGroupAccess(createdGroup.key, access);
        });
    }

    activateGroup(groupId: string, access: string) {
        this.afDatabase.object(`/${access}_groups/${groupId}/metadata`).update({status: 'active'});
    }

    getReceiverId(groupId: string): Observable<string> {
        const owner = this.authService.loggedInId;
        return this.afDatabase.object(`group_distrib/${groupId}/${owner}`).valueChanges()
            .map(val => {
                return val ? val.toString() : null;
            });
    }

    getUserGroups(): Observable<Group[]> {
        const owner = this.authService.loggedInId;
        return Observable.combineLatest(
                this.afDatabase.list(`/user_groups/${owner}/public`).snapshotChanges(),
                this.afDatabase.list(`/user_groups/${owner}/private`).snapshotChanges()
            )
            .do(console.log)
            .map(([first, second]) => {
                return [...first, ...second]
            })
            .map(snapshots => snapshots.map(snapshot => {
                return { key: snapshot.key, value: snapshot.payload.val() };
            }))
            .switchMap((groupIds) => {
                console.log(groupIds);
                if (groupIds.length === 0) {
                    return Observable.of([]);
                }
                return Observable.combineLatest(groupIds.map(id => {
                    const access = id.value.length > 0 ? 'private' : 'public';
                    return this.afDatabase.object(`/${access}_groups/${id.key}/metadata`).valueChanges()
                        .map(value => {
                            return {key: id.key, value};
                        })
                }));
            }).map((objs: any[]) => {
                console.log(objs);
                return objs.map(obj => {
                    return {
                        id: obj.key,
                        count: obj.value.count,
                        owner: obj.value.owner,
                        name: obj.value.name,
                        price: obj.value.price,
                        access: obj.value.access,
                        status: obj.status
                    }
                })
            });
    }

    getGroupUsers(groupId: string, access: string): Observable<User[]> {
        return this.afDatabase.list(`/group_users/${access}/${groupId}`).snapshotChanges()
            .map(snapshots => snapshots.map(snapshot => {
                return { key: snapshot.key, value: snapshot.payload.val() };
            }))
            .switchMap((userIds) => {
                console.log(userIds);
                return Observable.combineLatest(userIds.map(id => {
                    return this.afDatabase.object(`/users/${id.key}/`).valueChanges();
                }))
            }).map((objs: any[]) => {
                return objs.map(obj => {
                    return {displayName: obj.displayName, email: obj.email}
                })
            });
    }



    getGroupInfo(groupId: string): Observable<Group> {
        return this.afDatabase.object(`/group_access/${groupId}`)
            .snapshotChanges()
            .map(snapshot => {
                return { key: snapshot.key, value: snapshot.payload.val() };
            })
            .switchMap((groupAccess) => {
                return this.afDatabase.object<Group>(`${groupAccess.value}_groups/${groupId}/metadata`)
                    .valueChanges()
                    .map(obj => {
                        return {
                            id: groupId,
                            count: obj.count,
                            owner: obj.owner,
                            name: obj.name,
                            price: obj.price,
                            access: obj.access,
                            status: obj.status
                        }
                    });
            })
    }

    joinGroup(groupName: string, password: string = '') {
        const access: string = password.length > 0 ? 'private_groups' : 'public_groups';
        const userId = this.authService.loggedInId;
        this.afDatabase.list('/group_names/', ref => ref.orderByValue().equalTo(groupName))
            .snapshotChanges()
            .map(snapshots => {
                return snapshots.map(snapshot => {
                    return { key: snapshot.key, value: snapshot.payload.val() };
                })
            })
            .subscribe((group) => {
                this.updateUserGroups(userId, group[0].key, password, true);
                this.updateGroupUsers(userId, group[0].key, password, true);
            });
        // this.updateUserGroups(owner, groupId, , true);
    }

    joinGroupById(groupId: string, password: string = '') {
        const userId = this.authService.loggedInId;
        this.updateUserGroups(userId, groupId, password, true);
        this.updateGroupUsers(userId, groupId, password, true);
    }

    leaveGroup(groupId: string, access: string) {
        const owner = this.authService.loggedInId;
        this.updateUserGroups(owner, groupId, access === 'private' ? ' ' : '');
        this.updateGroupUsers(owner, groupId, access === 'private' ? ' ' : '');
    }

    // TODO create a rule - check if kicking user is group owner
    kickFromGroup(groupId: string, userToKick: string) {
        this.afDatabase.object(`/groups/${groupId}/users/${userToKick}`).remove();
        this.updateUserGroups(userToKick, groupId);
    }

    private pushGroupAccess(groupId: string, access: string) {
        this.afDatabase.object(`/group_access/${groupId}`).set(access);
    }

    private pushGroupName(groupId: string, groupName: string) {
        this.afDatabase.object(`/group_names/${groupId}`).set(groupName);
    }

    private updateUserGroups(userId: string, groupId: string, password: string = '', join: boolean = false) {
        const access: string = password.length > 0 ? 'private' : 'public';
        const path: string = `/user_groups/${userId}/${access}/${groupId}`;
        if (join) {
            this.afDatabase.object(path).set(password);
        } else {
            this.afDatabase.object(path).remove();
        }
    }

    private updateGroupUsers(userId: string, groupId: string, password: string = '', join: boolean = false) {
        const access: string = password.length > 0 ? 'private' : 'public';
        const path: string = `/group_users/${access}/${groupId}/${userId}`;
        if (join) {
            this.afDatabase.object(path).set(password);
        } else {
            this.afDatabase.object(path).remove();
        }
    }

}
