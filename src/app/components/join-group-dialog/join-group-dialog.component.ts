import {Component, OnInit, ViewChild} from '@angular/core';
import {MatAutocompleteTrigger, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GroupsService} from '../../services/groups.service';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-join-group-dialog',
  templateUrl: './join-group-dialog.component.html',
  styleUrls: ['./join-group-dialog.component.scss']
})
export class JoinGroupDialogComponent implements OnInit {
  @ViewChild(MatAutocompleteTrigger)
  private autoComplete: MatAutocompleteTrigger;

  private searchSymbol = '\uf8ff';
  private lastKeyPress = 0;

  groupsNames$: Observable<string[]>;
  start$: BehaviorSubject<string|null> = new BehaviorSubject('');
  end$: BehaviorSubject<string|null> = new BehaviorSubject(this.searchSymbol);

  public form: FormGroup;
  constructor(private groupsService: GroupsService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<JoinGroupDialogComponent>) {
      this.form = this.fb.group({
          name: ['', [Validators.required, Validators.minLength(5)]],
          password: ['']
      });
  }

  ngOnInit() {
      this.groupsNames$ = this.groupsService.getGroupNames(this.start$, this.end$);
  }

  onJoinClick() {
      if (this.form.valid) {
          console.log('valid');
          this.groupsService.joinGroup(this.form.value.name, this.form.value.password);
          this.dialogRef.close();
      } else {
          // TODO display warning
      }
  }

  onSearch(event) {

      if (event.timeStamp - this.lastKeyPress < 200) {
          return;
      }
      console.log(event);
      const searchText = event.target.value;

      console.log('Search', searchText);

      this.start$.next(searchText);
      this.end$.next(searchText + this.searchSymbol);

      this.lastKeyPress = event.timeStamp;
  }

}
