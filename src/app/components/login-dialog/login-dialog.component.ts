import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {RegisterDialogComponent} from '../register-dialog/register-dialog.component';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder, public dialog: MatDialog, private authService: AuthService) {
      this.form = this.fb.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', Validators.required]
      });
  }

  ngOnInit() {
  }

  onRegister() {
      const dialogRef = this.dialog.open(RegisterDialogComponent);

      dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
      });
  }

  onFacebookLogin() {
      this.authService.signInFacebook();
      this.dialog.closeAll();
  }

  onGoogleLogin() {
      this.authService.signInGoogle();
      this.dialog.closeAll();
  }

  onEmailLogin() {
      if (this.form.valid) {
          this.authService.signInEmail(this.form.value['email'], this.form.value['password']);
          this.dialog.closeAll();
      }
  }

}
