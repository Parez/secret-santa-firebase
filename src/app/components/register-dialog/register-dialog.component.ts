import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder, private authService: AuthService, public dialog: MatDialog) {
      this.form = this.fb.group({
          displayName: ['', [Validators.required]],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.min(6)]]
      });
  }

  signUp() {
      if (this.form.valid) {
          this.authService.registerByEmail(this.form.value['email'], this.form.value['password'], this.form.value['displayName']);
          this.dialog.closeAll();
      }
  }

  ngOnInit() {

  }

}
