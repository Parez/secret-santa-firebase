import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Group} from '../../models/group.model';
import {GroupsService} from '../../services/groups.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {PasswordDialogComponent} from '../password-dialog/password-dialog.component';
import {MatDialog} from '@angular/material';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.scss']
})
export class GroupItemComponent implements OnInit, OnDestroy {

  @Input()
  set group(val: Group) {
      console.log(val);
    if (val && this._group.id !== val.id) {
        console.log('Here again');
      this._group = val;
    }
  };
  get group(): Group {
      return this._group;
  };


  inGroupSub: Subscription;
  _group: Group = {id: '0'};
  inGroup: boolean = true;

  constructor(private router: Router,
              private groupsService: GroupsService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
      console.log('ID', this._group.id);
      this.inGroupSub = this.groupsService.isCurrentUserInGroup(this.group.id, this.group.access)
          .subscribe(inGroup => {
              console.log(inGroup);
              this.inGroup = inGroup;
          })
  }
  ngOnDestroy(): void {
      this.inGroupSub.unsubscribe();
  }

  viewGroup() {
    if (this.inGroup) {
        this.router.navigate([`group/${this.group.id}`]);
    }
  }

  leaveGroup() {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
          data: {action: `покинуть группу ${this.group.name}`}
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result.leave) {
              this.groupsService.leaveGroup(this.group.id, this.group.access);
          }
      });
  }

  joinGroup() {
    if (this.group.access === 'private') {
        const dialogRef = this.dialog.open(PasswordDialogComponent, {
          data: {name: this.group.name}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.password) {
              this.groupsService.joinGroupById(this.group.id, result.password);
          }
        });
    } else {
        this.groupsService.joinGroupById(this.group.id);
    }
  }

}
