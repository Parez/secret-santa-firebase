import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Input()
  set profile(val: User) {
    this._profile = val;
    if (val) {
        this.form = this.fb.group({
            displayName: [this.profile.displayName, [Validators.required]],
            interests: [this.profile.interests]
        });
    }
  }
  get profile(): User {
    return this._profile;
  }
  _profile: User;

  @Output() submit: EventEmitter<User> = new EventEmitter();

  form: FormGroup;
  constructor(private fb: FormBuilder) {
      this.form = this.fb.group({
          displayName: ['', [Validators.required]],
          interests: ['']
      });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submit.emit(this.form.value);
  }

}
