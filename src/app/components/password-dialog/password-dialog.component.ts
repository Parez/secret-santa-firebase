import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.scss']
})
export class PasswordDialogComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<PasswordDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
      this.form = this.fb.group({
          password: ['', [Validators.required]]
      });
  }

  ngOnInit() {
  }

  onJoinClick() {
    this.dialogRef.close({password: this.form.controls['password'].value});
  }

  onCancelClick() {
      this.dialogRef.close({});
  }

}
