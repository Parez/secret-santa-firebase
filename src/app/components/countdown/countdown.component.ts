import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CountdownService} from '../../services/countdown.service';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit {

  days$: Observable<number>;
  hours$: Observable<number>;
  minutes$: Observable<number>;
  seconds$: Observable<number>;

  constructor(private countService: CountdownService) {
      this.days$ = this.countService.countdown$.map(date => date.days());
      this.hours$ = this.countService.countdown$.map(date => date.hours());
      this.minutes$ = this.countService.countdown$.map(date => date.minutes());
      this.seconds$ = this.countService.countdown$.map(date => date.seconds());
  }

  ngOnInit() {

  }

}
