import {Component, Input, OnInit} from '@angular/core';
import {Group} from '../../models/group.model';
import {User} from '../../models/user.model';
import {AuthService} from '../../services/auth.service';
import {GroupsService} from '../../services/groups.service';
import {Subscription} from 'rxjs/Subscription';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  @Input()
  set group(val: Group) {
      console.log(val);
      if (val) {
          this._group = val;
          this.inGroupSub = this.groupsService.isCurrentUserInGroup(this.group.id, this.group.access)
              .subscribe(inGroup => {
                  console.log(inGroup);
                  this.inGroup = inGroup;
              });

          if (this._group.status === 'active') {
              this.receiver$ = this.groupsService.getReceiverId(this.group.id).switchMap(id => {
                  return this.authService.getUserInfo(id);
              });
          }
      } else {
        this._group = null;
      }
  };
  get group(): Group {
      return this._group;
  };
  _group: Group = {id: '0'};

  @Input() users: User[] = [];

  inGroupSub: Subscription;
  inGroup: boolean = true;

  receiver$: Observable<User>;
  constructor(public authService: AuthService,
              private groupsService: GroupsService,
              public dialog: MatDialog, private router: Router,) { }

  ngOnInit() {

  }

  leaveGroup() {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
          data: {action: `покинуть группу ${this.group.name}`}
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result.leave) {
              this.groupsService.leaveGroup(this.group.id, this.group.access);
              this.router.navigate(['/dashboard']);
          }
      });
  }

  onActivate() {
      this.groupsService.activateGroup(this.group.id, this.group.access);
  }

}
