import { Component, OnInit } from '@angular/core';
import {GroupsService} from '../../services/groups.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-create-group-dialog',
  templateUrl: './create-group-dialog.component.html',
  styleUrls: ['./create-group-dialog.component.scss']
})
export class CreateGroupDialogComponent implements OnInit {

  public form: FormGroup;
  constructor(private groupsService: GroupsService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateGroupDialogComponent>) {
      this.form = this.fb.group({
          name: ['', [Validators.required, Validators.minLength(5)]],
          password: [''],
          price: [500]
      });
  }

  ngOnInit() {
  }

  onCreateClick() {
      if (this.form.valid) {
          console.log('valid');
          this.groupsService.createGroup(this.form.value.name, this.form.value.password, this.form.value.price);
          this.dialogRef.close();
      } else {
          // TODO display warning
      }
  }

}
