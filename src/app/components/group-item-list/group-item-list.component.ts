import {Component, Input, OnInit} from '@angular/core';
import {Group} from '../../models/group.model';

@Component({
  selector: 'app-group-item-list',
  templateUrl: './group-item-list.component.html',
  styleUrls: ['./group-item-list.component.scss']
})
export class GroupItemListComponent implements OnInit {

  @Input() groups: Group[] = null;

  constructor() { }

  ngOnInit() {
  }

}
