import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupItemListComponent } from './group-item-list.component';

describe('RoomItemListComponent', () => {
  let component: GroupItemListComponent;
  let fixture: ComponentFixture<GroupItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
