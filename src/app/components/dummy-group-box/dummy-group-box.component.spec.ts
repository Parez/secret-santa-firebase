import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyGroupBoxComponent } from './dummy-group-box.component';

describe('DummyGroupBoxComponent', () => {
  let component: DummyGroupBoxComponent;
  let fixture: ComponentFixture<DummyGroupBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyGroupBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyGroupBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
