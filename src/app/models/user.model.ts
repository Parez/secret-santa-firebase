export interface User {
    displayName?: string;
    email?: string;
    id?: string;
    interests?: string;
}
