import {User} from './user.model';

export interface Group {
    id?: string;
    count?: number;
    name?: string;
    price?: number;
    owner?: User;
    access?: string;
    status?: string;
}
