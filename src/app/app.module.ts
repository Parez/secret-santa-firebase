import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import {MaterialModule} from './material/material.module';
import {AppRoutingModule} from './app-routing.module';
import {AuthService} from './services/auth.service';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {CountdownComponent} from './components/countdown/countdown.component';
import {CountdownService} from './services/countdown.service';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ParticlesModule} from 'angular-particle';
import {LoginDialogComponent} from './components/login-dialog/login-dialog.component';
import { RegisterDialogComponent } from './components/register-dialog/register-dialog.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import {AuthGuard} from './guards/auth.guard';
import {CanDeactivateHomeGuard} from './guards/can-deactivate-home.guard';
import { GroupItemComponent } from './components/group-item/group-item.component';
import { GroupItemListComponent } from './components/group-item-list/group-item-list.component';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { CreateGroupDialogComponent } from './components/create-group-dialog/create-group-dialog.component';
import {GroupsService} from './services/groups.service';
import { DummyGroupBoxComponent } from './components/dummy-group-box/dummy-group-box.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { JoinGroupDialogComponent } from './components/join-group-dialog/join-group-dialog.component';
import { GroupPageComponent } from './group-page/group-page.component';
import { GroupComponent } from './components/group/group.component';
import { UserItemComponent } from './components/user-item/user-item.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { GroupsListPageComponent } from './groups-list-page/groups-list-page.component';
import { PasswordDialogComponent } from './components/password-dialog/password-dialog.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { ProfileFormComponent } from './components/profile-form/profile-form.component';
import { BasePageComponent } from './base-page/base-page.component';
import {UserInGroupGuard} from './guards/user-in-group.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CountdownComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    DashboardPageComponent,
    GroupItemComponent,
    GroupItemListComponent,
    CreateGroupDialogComponent,
    DummyGroupBoxComponent,
    ProfilePageComponent,
    JoinGroupDialogComponent,
    GroupPageComponent,
    GroupComponent,
    UserItemComponent,
    UserListComponent,
    GroupsListPageComponent,
    PasswordDialogComponent,
    ConfirmDialogComponent,
    ProfileFormComponent,
    BasePageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FlexLayoutModule,
    ParticlesModule,
    ReactiveFormsModule
  ],
  entryComponents: [
      LoginDialogComponent,
      RegisterDialogComponent,
      CreateGroupDialogComponent,
      JoinGroupDialogComponent,
      PasswordDialogComponent,
      ConfirmDialogComponent
  ],
  providers: [
      AuthService,
      CountdownService,
      AuthGuard,
      UserInGroupGuard,
      CanDeactivateHomeGuard,
      GroupsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
