import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Group} from '../models/group.model';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {GroupsService} from '../services/groups.service';
import {User} from '../models/user.model';

@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss']
})
export class GroupPageComponent implements OnInit {

  groupInfo$: Observable<Group>;
  groupUsers$: Observable<User[]>;

  constructor(private route: ActivatedRoute, private groupsService: GroupsService) {
      this.groupInfo$ = this.route.paramMap.switchMap((params: ParamMap) => {
          const groupId = params.get('id');
          return groupsService.getGroupInfo(groupId);
      });

      this.groupUsers$ = this.groupInfo$.switchMap((group: Group) => {
          return groupsService.getGroupUsers(group.id, group.access);
      });
  }

  ngOnInit() {
  }

}
