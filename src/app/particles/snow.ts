export const snow = {
    particles: {
        number: {
            value: 200,
        },
        color: {
            value: '#ffffff'
        },
        shape: {
            type: 'circle',
        },
        line_linked: {
            enable: false
        },
        move: {
            enable: true,
            speed: 2,
            direction: 'bottom',
            straight: false,
            random: true
        }
    },
    interactivity: {
        detect_on: 'canvas',
        events: {
            onhover: {
                enable: true,
                mode: ['repulse']
            }
        },
        resize: true,
        modes: {
            bubble: {
                distance: 200,
                size: 5,
                duration: 1,
                opacity: 0.8,
                speed: 3
            },
            repulse: {
                distance: 200
            }
        }
    }
};