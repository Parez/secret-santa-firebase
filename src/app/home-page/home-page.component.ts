import { Component, OnInit } from '@angular/core';
import {snow} from '../particles/snow';
import {MatDialog} from '@angular/material';
import {LoginDialogComponent} from '../components/login-dialog/login-dialog.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;

  constructor(public dialog: MatDialog) {

  }



  ngOnInit() {
      this.myStyle = {
          'position': 'fixed',
          'width': '100%',
          'height': '100%',
          'z-index': 20,
          'top': 0,
          'left': 0,
          'right': 0,
          'bottom': 0,
      };

      this.myParams = snow;
  }

  onLoginClick() {
      const dialogRef = this.dialog.open(LoginDialogComponent, {
          height: '350px'
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
      });
  }

}
