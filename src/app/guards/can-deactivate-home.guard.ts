import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthService} from '../services/auth.service';
import {HomePageComponent} from '../home-page/home-page.component';

@Injectable()
export class CanDeactivateHomeGuard implements CanDeactivate<HomePageComponent> {

    constructor(public authService: AuthService) {}

    canDeactivate(component: HomePageComponent,
                  currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot,
                  nextState?: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        return this.authService.user$.map(user => user != null);
    }
}
