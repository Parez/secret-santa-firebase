import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {GroupsService} from '../services/groups.service';
import {AuthService} from '../services/auth.service';

@Injectable()
export class UserInGroupGuard implements CanActivate {

  constructor(private groupsService: GroupsService,  private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const groupId = next.params.id;
    const isIngroup$ = this.groupsService.getGroupAccess(groupId).switchMap(access => {
        return this.groupsService.isCurrentUserInGroup(groupId, access);
    });

    isIngroup$.subscribe(inGroup => {
        if (!inGroup) {
            this.router.navigate(['/dashboard']);
        }
    });

    return isIngroup$;
  }
}
