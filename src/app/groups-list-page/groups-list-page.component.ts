import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Group} from '../models/group.model';
import {GroupsService} from '../services/groups.service';

@Component({
  selector: 'app-groups-list-page',
  templateUrl: './groups-list-page.component.html',
  styleUrls: ['./groups-list-page.component.scss']
})
export class GroupsListPageComponent implements OnInit {

  groups$: Observable<Group[]>;

  constructor(private groupsService: GroupsService) {
    this.groups$ = this.groupsService.getAllGroups()
  }

  ngOnInit() {
  }

}
