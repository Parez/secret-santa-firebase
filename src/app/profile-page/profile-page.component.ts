import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../models/user.model';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  profile$: Observable<User>;
  constructor(public authService: AuthService) {
      this.profile$ = this.authService.getProfileInfo();
  }

  ngOnInit() {
  }

  onUpdateProfile(data) {
      this.authService.updateProfileInfo(data);
  }

}
