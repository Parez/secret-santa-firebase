const database = {
    'group_names': {
        'group1': 'swag2078',
        'group2': 'my_group1'
    },
    'public_groups': {
        'group1': {
            // read: all authenticated users
            // ".write": "auth !== null && (!data.exists() || data.child('metadata').child('owner').val() === auth.uid)"
            // allow create to all, but update only to the owner
            'metadata': {
                'name': 'swag2078',
                'owner': 'user1',
                'num_users': 2, // increase using cloud function
                'price': 300
                // ".validate": "data.owner == auth.uid"
            }
        }
    },
    'private_groups': {
        'group2': {
            // read: only by users who are members of the group
            // ".write": "auth !== null && (!data.exists() || data.child('metadata').child('owner').val() === auth.uid)"
            // allow create to all, but update only to the owner
            'metadata': {
                'name': 'my_group1',
                'owner': 'user1',
                'num_users': 2,
                'price': 500
            },
            'users': {
                // write: only if password matches
                // ".validate": "$user_id == auth.uid && newData.val() == root.child('/chattrooms/' + $room_id + '/password').val()"
                'user1': 'abc123',
                'user3': 'abc123'
            },
            'password': 'abc123'
        }
    },
    'group_users': {
        'public': {
            'group1': {
                'user1': true,
                'user2': true
                // ".validate": "auth.uid == $userId"
            }
        },
        'private': {
            'group2': {
                'user1': 'abc123',
                'user3': 'abc123'
                // ".validate": "$userId == auth.uid && newData.val() == root.child('/private_groups/' + $roomId + '/password').val()"
            }
        }
    },
    'user_groups': {
        'user1': {
            // read/write only by user1
            'group1': 'public',
            'group2': 'private'
        },
        'user2': {
            // read/write only by user2
            'group1': 'public'
        },
        'user3': {
            // read/write only by user3 (auth.uid === $userId)
            'group2': 'private'
        }
    }
};
