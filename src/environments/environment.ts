// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: 'AIzaSyBunQ6VyRCL0fCqZ37rvuRpNSStkMuvBJg',
      authDomain: 'santa-a2b15.firebaseapp.com',
      databaseURL: 'https://santa-a2b15.firebaseio.com',
      projectId: 'santa-a2b15',
      storageBucket: 'santa-a2b15.appspot.com',
      messagingSenderId: '741013056305'
  }
};
