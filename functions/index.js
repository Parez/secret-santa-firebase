const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.timeStamp = functions.https.onRequest((request, response) => {
    response.send(Date.now());
});

exports.secretSantaPrivate = functions.database.ref('/private_groups/{groupId}/metadata/status')
    .onWrite((event) => {
        const eventSnapshot = event.data;
        const status = event.data.val();

        if(status !== 'active') {
            return;
        }

        return admin.database().ref(`/group_users/private/${event.params.groupId}`).once('value')
            .then(groupUsers => {
                const santasAr = assignSantas(Object.keys(groupUsers.val()));
                const distribRef = eventSnapshot.adminRef.root
                    .child(`group_distrib`)
                    .child(event.params.groupId);

                const assignedUsers = santasAr.map(santas => {
                    return {[santas.santa]: santas.recipient}
                }).reduce((acc, idObj) => {
                    return Object.assign({}, acc, idObj);
                });

                return distribRef.set(assignedUsers);
            });
});

exports.secretSantaPublic = functions.database.ref('/public_groups/{groupId}/metadata/status')
    .onWrite((event) => {
        const eventSnapshot = event.data;
        const status = event.data.val();

        if(status !== 'active') {
            return;
        }

        return admin.database().ref(`/group_users/public/${event.params.groupId}`).once('value')
            .then(groupUsers => {
                const santasAr = assignSantas(Object.keys(groupUsers.val()));
                const distribRef = eventSnapshot.adminRef.root
                    .child(`group_distrib`)
                    .child(event.params.groupId);

                const assignedUsers = santasAr.map(santas => {
                    return {[santas.santa]: santas.recipient}
                }).reduce((acc, idObj) => {
                    return Object.assign({}, acc, idObj);
                });

                return distribRef.set(assignedUsers);
            });
    });

function assignSantas(array) {
    let matches = [];

    if(!array || !array.length) {
        return null;
    }

    let santas = array.slice();
    shuffle(santas);

    for(let i=0; i<santas.length; i++) {
        let santa = santas[i],
            recipient;

        // Assign santa to the person next to them to avoid assigning to self and avoid duplicate recipients
        if(i !== santas.length-1) {
            recipient = santas[i+1];
        } else {
            recipient = santas[0];
        }

        matches.push({ "santa": santa, "recipient": recipient });
    }

    return matches;
}

function shuffle(array) {
    let n = array.length, i, j;

    while(n) {
        i = Math.floor(Math.random() * n--);

        j = array[n];
        array[n] = array[i];
        array[i] = j;
    }
}

exports.updateUsersCount = functions.database.ref('/group_users/{access}/{groupId}/{userId}')
    .onWrite(event => {

        const eventSnapshot = event.data;

        const countRef = eventSnapshot.adminRef.root
            .child(`${event.params.access}_groups`)
            .child(event.params.groupId)
            .child('metadata')
            .child('count');

        return countRef.transaction(function(current) {
            if (event.data.exists() && !event.data.previous.exists()) {
                return (current || 0) + 1;
            }
            else if (!event.data.exists() && event.data.previous.exists()) {
                return (current || 0) - 1;
            }
        });
    });

